<html>
    <head>
        <title>Brevet Time</title>
    </head>

    <body>
        <h1>List of Brevet Time</h1>
        <ul>
            <?php
            $all = file_get_contents('http://laptop-service/listAll');
            echo "All Brevet Time:\n";
            foreach ($all as $t) {
                echo "<li>$t</li>";
            }   
    
            $all_of_csv = file_get_contents('http://laptop-service/listAll/csv');
            echo "All Brevet Time (CSV):\n";
            foreach ($all_of_csv as $t){
                echo "<li>$t</li>";
            }   

            $all_of_json = file_get_contents('http://laptop-service/listAll/json');
            echo "All Brevet Time (JSON):\n";
            foreach ($all_of_json as $t) {
                echo "<li>$t</li>";
            }   

            $csv_is_open = file_get_contents('http://laptop-service/listOpenOnly/csv');
            echo "Time Open (CSV):\n";
            foreach($csv_is_open as $t){
                echo "<li>$t</li>";
            }   

            $csv_is_close = file_get_contents('http://laptop-service/listCloseOnly/csv');
            echo "Time Close (CSV):\n";
            foreach($csv_is_close as $t){
                echo "<li>$t</li>";
            }   

            $json_is_open = file_get_contents('http://laptop-service/listOpenOnly/json');
            echo "Time Open (JSON):\n";
            foreach($json_is_open as $t){
                echo "<li>$t</li>";
            }   

            $json_is_close = file_get_contents('http://laptop-service/listCloseOnly/json');
            echo "Time Close (JSON):\n";
            foreach($json_is_close as $t){
                echo "<li>$t</li>";
           }   


            ?>  
        </ul>
    </body>
</html>
