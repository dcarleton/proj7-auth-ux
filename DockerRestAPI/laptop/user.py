from psw import verify_password
from psw import hash_password

class User():
    def __init__(self, username, password):
        self.username = username
        self.password = password
        db2.users.insert_one({"use4rname": self.username})

    def get_id(self):
        return self.username

    def is_active(self):
        return True

    def is_auth(self):
        return True

    def is_anon(self):
        return False

    @staticmethod
    def valid_login(password, num_of_hash):
        return verify_password(password, num_of_hash)
