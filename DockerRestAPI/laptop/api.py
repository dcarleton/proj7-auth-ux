# Laptop Service

from flask import Flask
from flask_restful import Resource, Api
from flask import request
import pymongo
from pymongo import MongoClient
from psw import hash_password
from psw import verify_password

# Instantiate the app
app = Flask(__name__)
api = Api(app)

login_manager = LoginManager()
login_manager.init_app(app)

app.config["SECRET_KEY"] = "123456"

client = MongoClient('db', 27017)
db = client.tododb
db2 = client.users

class all(Resource):

    def get(self):
        token = request.args.get('top')
        if token == None:
            return render_template("401.html"),401

        valid = User.verify_token(token, app.config["SECRET_KEY"])
        
        if valid == True:
            n = request.args.get("top")
            if n == None:
                top = 50
            else:
                top = int(n)

            _items = db.tododb.find().sort([("open_t",pymongo.ASCENDING), ("close_t",pymongo.ASCENDING)]).limit(top)
            list_of_items = [item for item in _items]
          
            app.logger.debug("Items")
            app.logger.debug(list_of_items)
          
            dict_of_items = {}
            open_time = []
            close_time = []
          
            for i in list_of_items:
            open_time.append(i["open_t"])
            close_time.append(i["close_t"])
  
            app.logger.debug("Open")
            app.logger.debug(open_time)
  
            app.logger.debug("Close")
            app.logger.debug(close_time)
 
            dict_of_items["open_t"] = open_time
            dict_of_items["close_t"] = close_time
            return dict_of_items

        if valid == False:
            return render_template("401.html"),401



api.add_resource(all, "/listAll")

class all_of_csv(Resource):

    def get(self):
        
        token = request.args.get("token")
        if token == None:
            return render_template("401.html"),401

        valid = User.verify_token(token,app.config["SECRET_KEY"]
        
        if valid == True: 
        
            n = request.args.get('top')
            if n == None:
                top = 50
            else:
                top = int(n)

            _items = db.tododb.find().sort([("open_t",pymongo.ASCENDING), ("close_t",pymongo.ASCENDING)]).limit(top)
            list_of_items = [item for item in _items]

            str_of_csv = ""
            for i in list_of_items:
                str_of_csv += i["open_t"]+", "
                str_of_csv += i["close_t"]+", "

            return str_of_csv

        if valid == False:
            return render_template("401.html"),401

api.add_resource(all_of_csv, "/listAll/csv")

class csv_is_open(Resource):

    def get(self):
        token = request.args.get('token')
        if token == None:
            return render_template("401.html")

        valid = User.verify_token(token, app.config["SECRET_KEY"])

        if valid == True:

            n = request.args.get("top")
            if n == None:
                top = 50
            else:
                top = int(n)

            _items = db.tododb.find().sort("open_t",pymongo.ASCENDING).limit(top)
            list_of_items = [item for item in _items]

            str_of_csv_open = ""
            for i in list_of_items:
                str_of_csv_open += i["open_t"]+", "

            return str_of_csv_open
    
        if valid == False:
            return render_template("401.html"),401

api.add_resource(csv_is_open, "/listOpenOnly/csv")

class csv_is_close(Resource):

    def get(self):

        token = request.args.get("token")
        if token == None:
            return render_template("401.html"),401

        valid = User.verify_token(token, app.config["SECRET_KEY"])
        
        if valid == True:

            n = request.args.get('top')
            if n == None:
                top = 50
            else:
                top = int(n)

            _items = db.tododb.find().sort("close_t",pymongo.ASCENDING).limit(top)
            list_of_items = [item for item in _items]

            str_of_csv_close = ""
            for i in list_of_items:
                str_of_csv_close += i["close_t"]+", "

            return str_of_csv_close

        if valid == False:
            return render_template("401.html"),401

api.add_resource(csv_is_close, "/listCloseOnly/csv")

class all_of_json(Resource):
    def get(self):
        token = request.args.get("token")
        if token == None:
            return render_template("401.html"),401

        valid = User.verify_token(token, app.config["SECRET_KEY"])

        if valid == True:

            n = request.args.get('top')
            if n == None:
                top = 50
            else:
                top = int(n)

            _items = db.tododb.find().sort([("open_t",pymongo.ASCENDING), ("close_t",pymongo.ASCENDING)]).limit(top)
            list_of_items = [item for item in _items]

            dict_of_items = {}
            open_time = []
            close_time = []

            for i in list_of_items:
                open_time.append(i["open_t"])
                close_time.append(i["close_t"])

            dict_of_items["open_t"] = open_time
            dict_of_items["close_t"] = close_time

            return dict_of_items

        if valid == False:
            return render_template("401.html"),401

api.add_resource(all_of_json, "/listAll/json")

class json_is_open(Resource):

    def get(self):

        token = request.args.get("token")
        if token == None:
            return render_template("401.html"),401

        valid = User.verify_token(token, app.config["SECRET_KEY"])

        if valid == True:
            n = request.args.get('top')
            if n == None:
                top = 50
            else:
                top = int(n)

            _items = db.tododb.find().sort("open_t",pymongo.ASCENDING).limit(top)
            list_of_items = [item for item in _items]

            dict_of_items = {}
            open_time = []
            close_time = []

            for i in list_of_items:
                open_time.append(i["open_t"])

            dict_of_items["open_t"] = open_time

            return dict_of_items 

        if valid == False:
            return render_template("401.html"),401

api.add_resource(json_is_open, "/listOpenOnly/json")

class json_is_close(Resource):

    def get(self):

        token = request.args.get("token")
        if token == None:
            return render_template("401.html"),401

        valid = User.verify_token(token, app.config["SECRET_KEY"]

        if valid == True:

            n = request.args.get('top')
            if n == None:
                top = 50
            else:
                top = int(n)

            _items = db.tododb.find().sort("close_t",pymongo.ASCENDING).limit(top)
            list_of_items = [item for item in _items]

            dict_of_items = {}
            open_time = []
            close_time = []

            for i in list_of_items:
                close_time.append(i["close_t"])

            dict_of_items["close_t"] = close_time

            return dict_of_items 

        if valid == False:
            return render_template("401.html"),401

api.add_resource(json_is_close, "/listCloseOnly/json")


class User():
    def __init__(self, username):
        self.username = username
    def get_id(self):
        return self.username
    def is_active(self):
        return True
    def is_auth(self):
        return True
    def is_anon(self):
        return False

    @staticmethod
    def create_auth_token(self, code, user_id, exp_date):
        ser = Serializer(code, user_id, exp_date)
        return ser.dumps(user_id)

    @staticmethod
    def valid_login(password, num_of_hash):
        return verify_password(password, num_of_hash)

    @staticmethod
    def verify_token(token, code):
        ser = Serializer(code)

        try:
            data = ser.loads(token)
        except BadSignature:
            return False
        except SignatureExpired:
            return False

        return True

class LoginForm(Form):
    username = StringField("username", validators = [DataRequired()])
    password = PasswordField("password", validators = [DataRequired()]

class RegisterForm(Form):
    username = StringField("username", [DataRequired(message="Enter your username")]
    password = PasswordField("password", [DataRequired(message="Enter your password")]

@login_manager.user_loader
def load_for_user(username):
    user = db2.users.find_one({"username": username})

    if user == None:
        return None
    
    return User(user["_id"])

@approute("/api/register", methods = ["GET", "POST"])
def register():
    form = RegisterForm(request.form)
    _items = db2.users.find()
    items = [item for item in _items]
    username = form.username.data
    password = form.password.data

    if form.validate() and request.method == "POST":
        user = db2.users.find_one("username": form.username.data})
        if user != None:
            flash("user is already in use")
            return render_template("401.html"), 401

        passed = hash_password(password)
        create_user = {"username": username, "password": passed}
        db2.users.insert_one(create_user)
        create_user = db2.users.find_one({"username:": username})
        output = {"username": create_user["username"], "password": create_user["password"], "_id": str(create_user["_id"])}

        return jsonify(result=result),201

return render_template("register.html", form=form)

@app.route("/api/token", methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)
    username = form.username.data
    password - form.password.data
    
    if form.validate() and request.method == "POST":
        db_for_user = db2.users.find_one({"username": username})
        
        if User.valid_login(form.password.data, db_for_user["password"]) and db_for_user != None:   
            db_user = User(str(db_for_user["_id"]))
            login_user(db_user, remember=True)
            flash("Valid", category="success")
            id_for_user = db_for_user["_id"]
            id = {"id": str(id_for_user)}
            token = user.create_auth_token(app.config["SECRET_KEY"], id, 600)
            token = token.decode("utf-8")
            token_result = {"token": token, "time:" 30}
            
            return jsonify(result=result),201

        return render_template("401.html"),401

    return render_template("login.html", form=form)

@app.route("/logout")
def logout():
    number = random.randint(1, 100)
    app.config["SECRET_KEY"] = app.config["SECRET_KEY"] + str(number)
    logout_user()
    return redirect(url_for("login"))




# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
