"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404 

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """ 
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    time = request.args.get('start_time', 999, type=str)
    date = request.args.get('start_date', 999, type=str)
    distance = request.args.get('brevet_distance', 999, type=int)

    app.logger.debug("km={}".format(km))
    app.logger.debug("start time={}".format(time))
    app.logger.debug("start date={}".format(date))
    app.logger.debug("request.args: {}".format(request.args))

## Initial given code
##    open_time = acp_times.open_time(km, 200, arrow.now().isoformat)
##    close_time = acp_times.close_time(km, 200, arrow.now().isoformat)
##    result = {"open": open_time, "close": close_time}

    ##what's above was the given incorrect code

    time_format = "{}T{}".format(date, time)
    t = arrow.get(time_format).isoformat()

    open_t = acp_times.open_time(km, distance, start)
    close_t = acp_times.close_time(km, distance, start)

    result = {"open_time": open_t, "close_time": close_t}
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    ##add secret key
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
