import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow
import logging
import config
import acp_times

app = Flask(__name__)

CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient('db', 27017)
db = client.tododb


@app.route('/')
@app.route('/index')
def index():
    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_calc_times")
def _calc_times():
    """ 
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    km = request.args.get('km', 999, type=float)
    time = request.args.get('start_time', 999, type=str)
    date = request.args.get('start_date', 999, type=str)
    distance = request.args.get('brevet_distance', 99, type=int)


## Initial given code
##    open_time = acp_times.open_time(km, 200, arrow.now().isoformat)
##    close_time = acp_times.close_time(km, 200, arrow.now().isoformat)
##    result = {"open": open_time, "close": close_time}

    ##what's above was the given incorrect code

    time_format = "{}T{}".format(date, time)
    t = (arrow.get(time_format)).isoformat()

    open_t = acp_times.open_time(km, distance, t)
    close_t = acp_times.close_time(km, distance, t)

    result = {"open_time": open_t, "close_time": close_t}
    return flask.jsonify(result=result)

@app.route('/nothing')
def nothing():
    return render_template('nothing.html')


@app.route('/todo', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('times.html', items=items)

@app.route('/new', methods=['POST'])
def new():

    total_open=[]
    total_close=[]
    items_open = request.form.getlist("open")
    items_close = request.form.getlist("close")
 
    for i in items_open:
        if str(i) != "": 
            items_open.append(str(i))
    for i in items_close:
        if str(i) != "": 
            items_close.append(str(i))

    for i in range(len(total_open)):
        item_doc = { 
            'name_open': items_open['i'],
            'name_close': items_close['i']
        }   
        db.tododb.insert_one(item_doc)
    
    _items = db.tododb.find()
    items = [item for item in _items]

    if items == []: 
        return redirect(url_for('nothing'))
    else:
        return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
